<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'findsa_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8)0mI[3ztl5fXV(%3A2JfjWK?D[tN9]@{>KYZ(d.aZ>rLtkV eKic=cR1G7{LT9{');
define('SECURE_AUTH_KEY',  '^pwE{SVjG04+o{^-uRWVvg&,VY8j(1p&Z~/O%h+&5g1b7z.h6$}_`^!E[7q-1x`,');
define('LOGGED_IN_KEY',    'V;*]+*}do59(kS_&zEdyUib%J$T=bIg)UG#AI )mG4aOPN-V8#TKsBrMX.I7fU3t');
define('NONCE_KEY',        '+*0ia3q(?%![;A1qyh*@>c[Of+#@Z^BS<tCg$lvj<ps_gH4{Eh& (ZJw`XFk,kdC');
define('AUTH_SALT',        '|jeO+ROE7pNJ6!q%1=U^8]y-(c4WP8UgxV)=!hDmMp)_2TGvLz}/%@UL)oYu{sAZ');
define('SECURE_AUTH_SALT', 'hb^Wc8jSS=`,=Eic?0d`nryi|-x;`1!L|s&yL:K1}jNPQAi#c~KI&[x}5xJ=BaKO');
define('LOGGED_IN_SALT',   '%%]ig_Sdg<%N^y`Vi]Rz%tV2ybTaKq|eK9m0r5&*t>[)fv$rznVals)f|t?$`(dW');
define('NONCE_SALT',       'dperv)hLRJY]DY<h$^Qo)>0[A`CrT/c+=;NTiEPE1P,`Em0fk[|5Dx<Q-f9r#)_s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
