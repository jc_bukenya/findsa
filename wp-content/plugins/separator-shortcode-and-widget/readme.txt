=== Separator Shortcode and Widget ===
Contributors: OTWthemes
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CQH6CMTR42KS2
Tags: separator, divider, shortcode, widgets, button, WYSIWYG editor, widget, sidebar
Requires at least: 3.6
Tested up to: 4.5
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create Separators / Dividers. Nice and easy interface. Insert anywhere in your site - page/post editor, sidebars, template files.

== Description ==

Easily create all different kinds of Separators / Dividers for your WordPress site. Insert Separators / Dividers anywhere in your site - page/post editor, sidebars, template files. No coding is required. It is all done in a nice and easy interface.

> Upgrade to the **Pro version** of this plugin - the fastes and easiest way to build your Wordpress based site including regular updates and premium support: 
> [Content Manager](http://codecanyon.net/item/content-manager-for-wordpress/7431829?ref=OTWthemes&utm_source=wp.org&utm_medium=page&utm_content=upgrade&utm_campaign=cml) | [Demo site](http://otwthemes.com/demos/1ts/?item=Content%20Manager&utm_source=wp.org&utm_medium=page&utm_content=upgrade&utm_campaign=cml) 

> Get some more awesome **FREE plugins**: <a href="https://wordpress.org/plugins/portfolio-manager-lite/" target="_blank">Portfolio Manager</a>, <a href="https://wordpress.org/plugins/widgetize-pages-light/" target="_blank">Widgetize Pages</a>, <a href="https://wordpress.org/plugins/blog-manager-light/" target="_blank">Blog Manager</a>

**Separators / Dividers Options**

* Margin Top and Bottom
* Display Line
* Text
* Text Alignment
* Link Text 
* Link URL
* Custom CSS Class

**Insert Separators / Dividers Anywhere in your site**

Basically Separators / Dividers can be inserted anywhere in your site

* Page/post WYSIWYG editor by using the buttion in the editor
* In sidebars by using the OTW Shortcode Widget
* In template files by using the Separator's shortcode and WordPress do_shortcode function

**Custom styling**

If you need to further style a Separators / Dividers here are your options:

* Each Separators / Divider shortcode has it's unique CSS class that can be used to style all Separator / Divider.
* Create a new class for each instance of a Separators / Dividers shortcode in its interface so you can style it individually.

**Localization/Internationalization**

This plugin comes Localization/Internationalization ready. It is following WordPress I18n standards.

**Full version of the plugin**

Upgrade to the full version of [Content Manager](http://codecanyon.net/item/content-manager-for-wordpress/7431829?ref=OTWthemes&utm_source=wp.org&utm_medium=page&utm_content=upgrade&utm_campaign=cml) |
[Demo site](http://otwthemes.com/demos/1ts/?item=Content%20Manager&utm_source=wp.org&utm_medium=page&utm_content=upgrade&utm_campaign=cml)

* Custom Responsive Layouts - Build in Seconds
* Front-end Editor - Edit your layouts and content in the front-end of your site
* 40+ Shortcodes with add/edit Interface, Custom and Imported Shortcodes
* Insert Shortcodes Anywhere - Layouts, Page Editor, Sidebars, Template files
* Insert Sidebars Anywhere - Layouts, Page Editor, Template files
* WordPress Widgets Anywhere - Layouts, Page Editor, Template files
* Content Sidebars
* Support and Updates
* Zero Coding Required

Follow on [Twitter](http://twitter.com/OTWthemes) |
[Facebook](http://www.facebook.com/pages/OTWthemes/250294028325665) |
[YouTube](http://www.youtube.com/OTWthemes) |
[Google +](https://plus.google.com/117222060323479158835/about)

== Installation ==

Please refer to [online documentation page](http://otwthemes.com/online-documentation-separator-shortcode-and-widget-plugin/?utm_source=wp.org&utm_medium=page&utm_content=docs&utm_campaign=cml) for more details.

== Frequently asked questions ==

Please refer to [online documentation page](http://otwthemes.com/online-documentation-separator-shortcode-and-widget-plugin/?utm_source=wp.org&utm_medium=page&utm_content=docs&utm_campaign=cml) for more details.

== Screenshots ==

1. WYSIWYG page/post editor button
2. OTW Shortcode Widget
3. Separator Options
4. Front-end

== Changelog ==

= 1.4 =

* Updated: OTW core updates

= 1.3 =

* Updated: javascript shortcodes settings optimization

= 1.2 =
 
* Fixed: wp link wrap zindex added

= 1.1 =

* Fixed: Adjustments on the options popup window

= 1.0 =

* Initial release